#! /bin/bash

#/cctbx/modules/cctbx_project/libtbx/env_config.py:207 need to have a 'local' prefix
#https://stackoverflow.com/questions/8227120/strange-local-folder-inside-virtualenv-folder

#mkdir modules

#wget http://cci.lbl.gov/repositories/annlib.gz
#wget http://cci.lbl.gov/repositories/ccp4io.gz
#wget http://cci.lbl.gov/repositories/clipper.gz
#wget http://cci.lbl.gov/repositories/eigen.gz
#wget http://cci.lbl.gov/repositories/ccp4io_adaptbx.gz
#wget https://github.com/SCons/scons/archive/3.1.1.zip
#wget http://cci.lbl.gov/repositories/tntbx.gz

virtualenv -p python2.7 venvpy2
. venvpy2/bin/activate
pip install six future numpy


cd modules

for name in annlib ccp4io clipper eigen ccp4io_adaptbx tntbx; do
tar xvzf $name.gz
done

unzip -n scons-3.1.1.zip
ln -s scons-3.1.1 scons

git clone https://github.com/cctbx/cctbx_project.git
git clone https://github.com/cctbx/boost.git
git clone https://github.com/cctbx/annlib_adaptbx.git

mkdir ../build_py2
cd ../build_py2

sed -i -e 's/\/include\/python/\/local\/include\/python/' /cctbx/modules/cctbx_project/libtbx/env_config.py

CXXFLAGS=-fpermissive python2.7 ../modules/cctbx_project/libtbx/configure.py --use_environment_flags annlib mmtbx
# it's ocassionally killed with more than one thread (low default sys resources?)
./bin/libtbx.scons -j 1

